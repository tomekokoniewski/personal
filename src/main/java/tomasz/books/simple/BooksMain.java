package tomasz.books.simple;

import java.util.Scanner;

/**
 * Simple Books Manager
 *
 * @author tomek
 */
public class BooksMain {

    public static void main(String[] args) {

        String[] books = new String[20];
        String[] authors = new String[20];
        Scanner scanner = new Scanner(System.in);
        int menuResponse;
        boolean dontStop = true;

        while (dontStop) {
            System.out.println("--------- MENU -----------------------------");
            System.out.println("Proszę wprowadzić cyfrę odpowiadającą menu");
            System.out.println("[1] - Add a book");
            System.out.println("[2] - Print book");
            System.out.println("[3] - Delete selected book");
            System.out.println("[4] - Delete all books");
            System.out.println("[5] - Find and print books by author");
            System.out.println("[6] - Count all books");
            System.out.println("[7] - Find first longest title");
            System.out.println("[9] - Exit the program");
            System.out.println("--------------------------------------------");

            menuResponse = scanner.nextInt();

            switch (menuResponse) {
                case 1:
                    addBook(books, authors, scanner);
                    break;
                case 2:
                    print(books, authors, scanner);
                    break;
                case 3:
                    remove(books, authors, scanner);
                    break;
                case 4:
                    remove(books, authors);
                    break;
                case 5:
                    findByAuthor(books, authors, scanner);
                    break;
                case 6:
                    count(books);
                    break;
                case 7:
                    findLongestTitle(books);
                    break;
                case 9:
                    dontStop = false;
                    break;
            }
        }
    }

    private static void addBook(String[] books, String[] authors, Scanner scanner) {
        System.out.println("___Add a book___");
        scanner.skip("\n");
        System.out.println("Input title of book");
        String title = scanner.nextLine();
        System.out.println("Input an author of book");
        String author = scanner.nextLine();
        System.out.println("Input position of book");
        int pos = scanner.nextInt();
        books[pos] = title;
        authors[pos] = author;
        System.out.println("Book '" + title + "', Author '" + author + "' added at position: " + pos);
    }

    private static void print(String[] books, String[] authors, Scanner scanner) {
        System.out.println("___Print book___");
        System.out.println("Input book's position to print");
        int pos = scanner.nextInt();
        System.out.println("Position: " + pos + " - Book's details:");
        System.out.println("Title: " + books[pos] + " Author: " + authors[pos]);
    }

    private static void remove(String[] books, String[] authors, Scanner scanner) {
        System.out.println("___Delete selected book___");
        System.out.println("Input book's position to delete");
        int pos = scanner.nextInt();
        if (books[pos] == null) {
            System.out.println("You cannot delete boot at position " + pos + "\nThere's no book");
        } else {
            System.out.println("Book: Title: " + books[pos] + " Author: " + authors[pos] + "\n Pernamently deleted");
            books[pos] = null;
            authors[pos] = null;
        }
    }

    private static void remove(String[] books, String[] authors) {
        System.out.println("___All books pernamently deleted___");
        for (int i = 0; i < books.length; i++) {
            books[i] = null;
            authors[i] = null;
        }
    }

    private static void findByAuthor(String[] books, String[] authors, Scanner scanner) {
        System.out.println("___Find and print books by author__");
        System.out.println("Input author to search his books");
        scanner.skip("\n");
        String author = scanner.nextLine();
        System.out.println("Books of " + author);

        for (int i = 0; i < authors.length; i++) {
            if ((authors[i] != null) && authors[i].equals(author)) {
                System.out.println(books[i]);
            }
        }
    }

    private static void count(String[] books) {
        System.out.println("___Count all books___");
        int quantity = 0;
        for (int i = 0; i < books.length; i++) {
            if ((books[i] != null)) {
                quantity++;
            }
        }

        System.out.println("There's: " + quantity + "books");
    }

    private static void findLongestTitle(String[] books) {
        System.out.println("___Find first longest title___");
        int size = 0;
        int pos = 0;
        for (int i = 0; i < books.length; i++) {
            if ((books[i] != null) && (books[i].length() > size)) {
                size = books[i].length();
                pos = i;
            }
        }
        if (pos > 0) {
            System.out.println("The longest title of book: " + books[pos]);
        } else {
            System.out.println("There's no books");
        }
    }
}
